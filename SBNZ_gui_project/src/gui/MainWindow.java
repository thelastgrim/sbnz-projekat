package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.NumberFormatter;

import com.sample.bean.Bolest;
import com.sample.bean.Dijagnoza;
import com.sample.bean.Pacijent;
import com.sample.bean.PacijentInfo;
import com.sample.bean.TerapijaInfo;

import functionalities.Bridge;
import templateModel.SearchByDateTemplateModel;
import templateModel.SearchByNameTemplateModel;

public class MainWindow {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JTextField textField_16;
	private JTextField textField_17;
	private JTextField textField_18;
	private JTextField textField_19;
	private JTextField textField_20;
	private JTextField textField_21;
	private JTextField textField_22;
	private JTextField textField_23;
	private JPanel panel_1;
	
	private HashMap<String, Float> found_tests;
	private JTextField textField_24;
	private JTextField textField_25;
	private JTextField textField_26;
	private JTextField textField_27;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void collectParams() {
		found_tests = new HashMap<String, Float>();
			
		for (int i = 0; i < panel_1.getComponentCount(); i++) {
			if(panel_1.getComponent(i) instanceof JTextField) {
				
				JTextField temp = (JTextField) panel_1.getComponent(i);
				if(!temp.getText().equals("")){
					Float value = Float.valueOf(temp.getText());
					JLabel tempLabel = (JLabel)panel_1.getComponent(i-1);
					String test_name = tempLabel.getText();
					found_tests.put(test_name, value);
					
				}
				
			}else {
				JLabel tempLabel = (JLabel)panel_1.getComponent(i);
				//System.out.println(tempLabel.getText());
			}
		}
		
	}
	
	private void initialize() {
		
		NumberFormat format = NumberFormat.getInstance();
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(1);
		formatter.setMaximum(125);
		formatter.setAllowsInvalid(false);
		 // If you want the value to be committed on each keystroke instead of focus lost
		formatter.setCommitsOnValidEdit(true);
		    
		frame = new JFrame();
		frame.setBounds(280, 20, 800, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{439, 0};
		gridBagLayout.rowHeights = new int[]{155, 219, 0, 219, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Informacije o pacijentu", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, -12, 0);
		gbc_panel.weightx = 30.0;
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		frame.getContentPane().add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 6, 1, 0));
		
		JLabel lblImeIPrezime = new JLabel("JMBG:");
		lblImeIPrezime.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(lblImeIPrezime);
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblPol = new JLabel("Pol:");
		lblPol.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(lblPol);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"M", "�"}));
		panel.add(comboBox);
		
		JLabel lblNewLabel = new JLabel("Starost:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(lblNewLabel);
		
		JFormattedTextField formattedTextField = new JFormattedTextField(formatter);
		panel.add(formattedTextField);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Hematolo\u0161ke analize", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.insets = new Insets(9, 0, 23, 0);
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		frame.getContentPane().add(panel_1, gbc_panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_1 = new JLabel("Leukociti");
		panel_1.add(lblNewLabel_1);
		
		textField_1 = new JTextField();
		panel_1.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblEritrociti = new JLabel("Eritrociti");
		panel_1.add(lblEritrociti);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		panel_1.add(textField_2);
		
		JLabel lblHemoglobin = new JLabel("Hemoglobin");
		panel_1.add(lblHemoglobin);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		panel_1.add(textField_3);
		
		JLabel lblHematokrit = new JLabel("Hematokrit");
		panel_1.add(lblHematokrit);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		panel_1.add(textField_4);
		
		JLabel lblMcv = new JLabel("MCV");
		panel_1.add(lblMcv);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		panel_1.add(textField_5);
		
		JLabel lblMch = new JLabel("MCH");
		panel_1.add(lblMch);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		panel_1.add(textField_6);
		
		JLabel lblMchc = new JLabel("MCHC");
		panel_1.add(lblMchc);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		panel_1.add(textField_7);
		
		JLabel lblRdw = new JLabel("RDW");
		panel_1.add(lblRdw);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		panel_1.add(textField_8);
		
		JLabel lblTrombociti = new JLabel("MPV");
		panel_1.add(lblTrombociti);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		panel_1.add(textField_9);
		
		JLabel lblPdw = new JLabel("PDW");
		panel_1.add(lblPdw);
		
		textField_10 = new JTextField();
		textField_10.setColumns(10);
		panel_1.add(textField_10);
		
		JLabel lblPct = new JLabel("PCT");
		panel_1.add(lblPct);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		panel_1.add(textField_11);
		
		JLabel lblNeut = new JLabel("NEUT%");
		panel_1.add(lblNeut);
		
		textField_12 = new JTextField();
		textField_12.setColumns(10);
		panel_1.add(textField_12);
		
		JLabel lblLym = new JLabel("LYM%");
		panel_1.add(lblLym);
		
		textField_14 = new JTextField();
		textField_14.setColumns(10);
		panel_1.add(textField_14);
		
		JLabel lblMono = new JLabel("MONO%");
		panel_1.add(lblMono);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		panel_1.add(textField_13);
		
		JLabel lblEos = new JLabel("EOS%");
		panel_1.add(lblEos);
		
		textField_15 = new JTextField();
		textField_15.setColumns(10);
		panel_1.add(textField_15);
		
		JLabel lblBaso = new JLabel("BASO%");
		panel_1.add(lblBaso);
		
		textField_16 = new JTextField();
		textField_16.setColumns(10);
		panel_1.add(textField_16);
		
		JLabel lblLuc = new JLabel("LUC%");
		panel_1.add(lblLuc);
		
		textField_17 = new JTextField();
		textField_17.setColumns(10);
		panel_1.add(textField_17);
		
		JLabel lblNeut_1 = new JLabel("NEUT#");
		panel_1.add(lblNeut_1);
		
		textField_18 = new JTextField();
		textField_18.setColumns(10);
		panel_1.add(textField_18);
		
		JLabel lblLym_1 = new JLabel("LYM#");
		panel_1.add(lblLym_1);
		
		textField_19 = new JTextField();
		textField_19.setColumns(10);
		panel_1.add(textField_19);
		
		JLabel lblMono_1 = new JLabel("MONO#");
		panel_1.add(lblMono_1);
		
		textField_21 = new JTextField();
		textField_21.setColumns(10);
		panel_1.add(textField_21);
		
		JLabel lblEos_1 = new JLabel("EOS#");
		panel_1.add(lblEos_1);
		
		textField_20 = new JTextField();
		textField_20.setColumns(10);
		panel_1.add(textField_20);
		
		JLabel lblBaso_1 = new JLabel("BASO#");
		panel_1.add(lblBaso_1);
		
		textField_22 = new JTextField();
		textField_22.setColumns(10);
		panel_1.add(textField_22);
		
		JLabel lblLuc_1 = new JLabel("LUC#");
		panel_1.add(lblLuc_1);
		
		textField_23 = new JTextField();
		textField_23.setColumns(10);
		panel_1.add(textField_23);
		
		JButton btnAnaliziraj = new JButton("Analiziraj");
		btnAnaliziraj.setToolTipText("");
		btnAnaliziraj.setPreferredSize(new Dimension(0, 40));
		GridBagConstraints gbc_btnAnaliziraj = new GridBagConstraints();
		gbc_btnAnaliziraj.insets = new Insets(-23, 0, -8, 0);
		gbc_btnAnaliziraj.fill = GridBagConstraints.VERTICAL;
		gbc_btnAnaliziraj.gridx = 0;
		gbc_btnAnaliziraj.gridy = 2;
		frame.getContentPane().add(btnAnaliziraj, gbc_btnAnaliziraj);
		btnAnaliziraj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				collectParams();
				PacijentInfo pi = new PacijentInfo(String.valueOf(comboBox.getSelectedItem()), Integer.valueOf(formattedTextField.getText()), textField.getText());
				Results results = new Results();
				Pacijent p = Bridge.calculate(found_tests, pi);
				if (p.getTrenutnaBolest().getTerapija()==null) {
					results.lblNewLabel.setText("Potrebno odraditi dodatne analize.");
					results.lblNewLabel_1.setText("Nema");
				}else {
				
					results.lblNewLabel.setText(p.getTrenutnaBolest().getBolest().getNaziv());
					results.lblNewLabel_1.setText(p.getTrenutnaBolest().getTerapija().getLekoviNames());
				}
				
				
				results.setVisible(true);;
			}
		});
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Izvestaj i pretraga", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(8, 0, 0, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 3;
		frame.getContentPane().add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] {0, 30, 30, 0, 0, 0, 0, 0};
		gbl_panel_2.rowHeights = new int[] {0, 0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JLabel lblNewLabel_5 = new JLabel("Unesite naziv bolesti");
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.gridwidth = 2;
		gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 0;
		panel_2.add(lblNewLabel_5, gbc_lblNewLabel_5);
		
		textField_27 = new JTextField();
		GridBagConstraints gbc_textField_27 = new GridBagConstraints();
		gbc_textField_27.gridwidth = 2;
		gbc_textField_27.insets = new Insets(5, 0, 5, 5);
		gbc_textField_27.fill = GridBagConstraints.BOTH;
		gbc_textField_27.gridx = 2;
		gbc_textField_27.gridy = 0;
		panel_2.add(textField_27, gbc_textField_27);
		textField_27.setColumns(10);
		
		JButton pretraziPoNazivu = new JButton("Pretrazi");
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.insets = new Insets(3, 0, 5, 5);
		gbc_btnNewButton_1.gridx = 4;
		gbc_btnNewButton_1.gridy = 0;
		panel_2.add(pretraziPoNazivu, gbc_btnNewButton_1);
		
		pretraziPoNazivu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				collectParams();
				SearchByNameTemplateModel term = new SearchByNameTemplateModel(textField_27.getText().toString());
				SearchResult results = new SearchResult();
				List<Bolest> rezultat = Bridge.pretraziBolesti(term);
				if(rezultat.size()==0) {
					results.textArea.setText("\nNije pronadjena nijedan pojam pod nazivom: "+ textField_27.getText().toString());
				}else {
					String prikaz = "";
					for(Bolest b: rezultat) {
						prikaz += b.toString()+"\n";
					}
					results.textArea.setText(prikaz);

				}
				
				
				
				results.setVisible(true);
			}
		});
		
		Icon icon = new ImageIcon("C:\\Users\\Aleksandra\\Documents\\GitHub\\sbnz-projekat\\SBNZ_gui_project\\src\\resources\\report21.jpg");
		JButton izvestajBtn = new JButton(icon);
		izvestajBtn.setToolTipText("Izvestaj o najpreporucenijoj i najmanje preporucenoj terapiji\r\n");
		izvestajBtn.setBorder(new MatteBorder(1, 1, 1, 1, (Color) null));
		
		izvestajBtn.setSize(150, 150);
		GridBagConstraints gbc_btnNewButton_2 = new GridBagConstraints();
		gbc_btnNewButton_2.anchor = GridBagConstraints.NORTHEAST;
		gbc_btnNewButton_2.insets = new Insets(-9, 0, 5, -3);
		gbc_btnNewButton_2.gridx = 7;
		gbc_btnNewButton_2.gridy = 0;
		panel_2.add(izvestajBtn, gbc_btnNewButton_2);
		izvestajBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				collectParams();
				Report results = new Report();
				HashMap<String,TerapijaInfo> rezultat = Bridge.izvestaj();
				
				String prikaz = "  Najpreporucenija terapija:\n          "+rezultat.get("najpreporucenija").getTerapija()+ "          Preporucena "+ rezultat.get("najpreporucenija").getBrPojavljivanja() + " put(a)\n\n";
				prikaz+="  Najmanje preporucena terapija:\n          "+rezultat.get("najmanjePreporucena").getTerapija() + "          Preporucena "+ rezultat.get("najmanjePreporucena").getBrPojavljivanja() + " put(a)\n";								
				results.textArea.setText(prikaz);
				results.setVisible(true);
			}
		});
		
		JLabel lblNewLabel_2 = new JLabel("JMBG:");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 1;
		panel_2.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		textField_24 = new JTextField();
		GridBagConstraints gbc_textField_24 = new GridBagConstraints();
		gbc_textField_24.gridwidth = 2;
		gbc_textField_24.insets = new Insets(5, 0, 0, 5);
		gbc_textField_24.fill = GridBagConstraints.BOTH;
		gbc_textField_24.gridx = 1;
		gbc_textField_24.gridy = 1;
		panel_2.add(textField_24, gbc_textField_24);
		textField_24.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Pocetni datum:");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_3.gridx = 3;
		gbc_lblNewLabel_3.gridy = 1;
		panel_2.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		textField_25 = new JTextField();
		GridBagConstraints gbc_textField_25 = new GridBagConstraints();
		gbc_textField_25.insets = new Insets(5, 0, 0, 5);
		gbc_textField_25.fill = GridBagConstraints.BOTH;
		gbc_textField_25.gridx = 4;
		gbc_textField_25.gridy = 1;
		panel_2.add(textField_25, gbc_textField_25);
		textField_25.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Krajnji datum:");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_4.gridx = 5;
		gbc_lblNewLabel_4.gridy = 1;
		panel_2.add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		textField_26 = new JTextField();
		GridBagConstraints gbc_textField_26 = new GridBagConstraints();
		gbc_textField_26.insets = new Insets(5, 0, 0, 5);
		gbc_textField_26.fill = GridBagConstraints.BOTH;
		gbc_textField_26.gridx = 6;
		gbc_textField_26.gridy = 1;
		panel_2.add(textField_26, gbc_textField_26);
		textField_26.setColumns(10);
		
		JButton pretraziIstoriju = new JButton("Pretrazi istoriju bolesti");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(5, 0, 0, 0);
		gbc_btnNewButton.gridx = 7;
		gbc_btnNewButton.gridy = 1;
		panel_2.add(pretraziIstoriju, gbc_btnNewButton);
		
		pretraziIstoriju.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				collectParams();
				SearchByDateTemplateModel dates = new SearchByDateTemplateModel(textField_25.getText().toString(), textField_26.getText().toString());
				PacijentInfo pi = new PacijentInfo(textField_24.getText().toString());
				SearchResult results = new SearchResult();
			//	Pacijent p = Bridge.calculate(found_tests, pi);
				List<Dijagnoza> rezultat = Bridge.pretraziIstorijuBolesti(dates, pi);
				if(rezultat == null) {
					results.textArea.setText("\nNe postoji pacijent sa JMBG-om: "+ textField_24.getText().toString());
				}
				
				else if(rezultat.size()==0) {
					results.textArea.setText("\nNe postoji istorija bolesti u periodu od "+ textField_25.getText().toString() + " do " + textField_26.getText().toString());
				}else {
					String prikaz = "";
					for(Dijagnoza d: rezultat) {
						prikaz += d.toString()+"\n";
					}
					results.textArea.setText(prikaz);

				}
								
				results.setVisible(true);
			}
		});
	}

}

