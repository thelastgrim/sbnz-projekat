package functionalities;

import java.util.HashMap;
import java.util.List;

import com.sample.DroolsTest;
import com.sample.bean.Bolest;
import com.sample.bean.Dijagnoza;
import com.sample.bean.Pacijent;
import com.sample.bean.PacijentInfo;
import com.sample.bean.TerapijaInfo;

import templateModel.SearchByDateTemplateModel;
import templateModel.SearchByNameTemplateModel;

public final class Bridge {
	
	public static Pacijent calculate(HashMap<String, Float> found_tests, PacijentInfo pi) {
		
		return DroolsTest.main(found_tests, pi);
	}
	
	public static List<Bolest> pretraziBolesti(SearchByNameTemplateModel term) {
		System.out.println("TERM: " + term.getNaziv());
		return DroolsTest.pretragaBolesti(term);
	}
	
	public static List<Dijagnoza> pretraziIstorijuBolesti(SearchByDateTemplateModel dates, PacijentInfo pi) {
		System.out.println("Pocetni datum: " + dates.getPocetniDatum() + "   krajnji datum: " + dates.getKrajnjiDatum() + "    JMBG: " + pi.getJMBG());
		return DroolsTest.pretragaIstorijeBolesti(dates, pi);
	}
	
	public static HashMap<String,TerapijaInfo> izvestaj() {
		System.out.println("bridge izvestaj");
		return DroolsTest.izvestaj();
	}
	
	
	

}
