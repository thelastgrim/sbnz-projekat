package com.same.testing2;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
//import static org.junit.jupiter.api.Assertions.*;
//import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

import com.sample.bean.Bolest;
import com.sample.bean.Dijagnoza;
import com.sample.bean.Lek;
import com.sample.bean.Pacijent;
import com.sample.bean.Status;
import com.sample.bean.Terapija;
import com.sample.bean.TipLeka;

public class TerapijaTest {
	
	KieServices ks = KieServices.Factory.get();
    KieContainer kContainer = ks.getKieClasspathContainer();
	KieSession kSession = kContainer.newKieSession("ksession-rules");
	

	@Test
	public void testTerapija() {
		Bolest b = new Bolest("Policitemija rubra vera");
    	b.getSimptomi().put("Eritrociti", Status.VERY_HIGH);
    	b.getSimptomi().put("Hematokrit", Status.HIGH);
    	
    	Bolest bo = new Bolest("Tromboza");
    	bo.getSimptomi().put("Eritrociti", Status.VERY_HIGH);
    	bo.getSimptomi().put("Hematokrit", Status.HIGH);
    	
    	
    	
    	Lek l = new Lek("Roferon",12);
    	Lek ll = new Lek("Lek br.2",18);
    	List<Lek> listaZaBazu = new ArrayList<>();
    	listaZaBazu.add(l);
    	listaZaBazu.add(ll);

    	
    	Lek l2 = new Lek("Farin",15);
    	Lek ll2 = new Lek("Lek br.2",18);
    	List<Lek> listaZaBazu2 = new ArrayList<>();
    	listaZaBazu2.add(l2);
    	listaZaBazu2.add(ll2);

    	Terapija te = new Terapija(listaZaBazu);
    	Terapija te2 = new Terapija(listaZaBazu2);
    	
    	Dijagnoza dijZaBazu = new Dijagnoza(b,te);
    	Dijagnoza dijZaBazu2 = new Dijagnoza(bo,te2);
    	
    	Dijagnoza dij = new Dijagnoza(bo, null);
    	ArrayList<com.sample.bean.Test> testoviTrenutni = napraviTrenutneTestove();
    	dij.setRezultati_nalaza(testoviTrenutni);
   // 	dij.setRezultati_nalaza(testoviTrenutni);
    	Pacijent pac = new Pacijent();
    	pac.setBrojGodina(18);
    	pac.setTrenutnaBolest(dij);
    	List<Dijagnoza> istorija = naraviIstorijuBolesti();
    	pac.setIstorijaBolesti(istorija);
    	kSession.insert(dijZaBazu);
		kSession.insert(dijZaBazu2);
		kSession.insert(pac);
		kSession.fireAllRules();
		
		QueryResults results = kSession.getQueryResults("getPacijent");
		
		Pacijent temp = null;
		for(QueryResultsRow row: results) {
			temp = (Pacijent) row.get("$result");
		}
    	
		assertNotNull(temp);
		assertEquals("Tromboza", temp.getTrenutnaBolest().getBolest().getNaziv());
		assertEquals("Farin", pac.getTrenutnaBolest().getTerapija().getLekovi().get(0).getNaziv());
    	
	}
	
	
	@Test
	public void testTerapijaNaOsnovuUzrasta() {
		Bolest b = new Bolest("Policitemija rubra vera");
    	b.getSimptomi().put("Eritrociti", Status.VERY_HIGH);
    	b.getSimptomi().put("Hematokrit", Status.HIGH);
    	
    	Bolest bo = new Bolest("Tromboza");
    	bo.getSimptomi().put("Eritrociti", Status.VERY_HIGH);
    	bo.getSimptomi().put("Hematokrit", Status.HIGH);
    	
    	
    	
    	Lek l = new Lek("Roferon",12);
    	Lek ll = new Lek("Lek br.2",18);
    	List<Lek> listaZaBazu = new ArrayList<>();
    	listaZaBazu.add(l);
    	listaZaBazu.add(ll);

    	
    	Lek l2 = new Lek("Farin",15);
    	Lek ll2 = new Lek("Lek br.2",18);
    	List<Lek> listaZaBazu2 = new ArrayList<>();
    	listaZaBazu2.add(l2);
    	listaZaBazu2.add(ll2);

    	Terapija te = new Terapija(listaZaBazu);
    	Terapija te2 = new Terapija(listaZaBazu2);
    	
    	Dijagnoza dijZaBazu = new Dijagnoza(b,te);
    	Dijagnoza dijZaBazu2 = new Dijagnoza(bo,te2);
    	
    	Dijagnoza dij = new Dijagnoza(bo, te2);
    	ArrayList<com.sample.bean.Test> testoviTrenutni = napraviTrenutneTestove();
    	dij.setRezultati_nalaza(testoviTrenutni);
    	Pacijent pac = new Pacijent();
    	pac.setBrojGodina(18);
    	pac.setTrenutnaBolest(dij);
    	List<Dijagnoza> istorija = naraviIstorijuBolesti();
    	pac.setIstorijaBolesti(istorija);
    	kSession.insert(dijZaBazu);
		kSession.insert(dijZaBazu2);
		kSession.insert(pac);
		kSession.fireAllRules();
		
		QueryResults results = kSession.getQueryResults("getPacijent");
		
		Pacijent temp = null;
		for(QueryResultsRow row: results) {
			temp = (Pacijent) row.get("$result");
		}
    	
		assertNotNull(temp);
		assertEquals("Tromboza", temp.getTrenutnaBolest().getBolest().getNaziv());
		assertEquals(1,  pac.getTrenutnaBolest().getTerapija().getLekovi().size());
		assertEquals("Farin", pac.getTrenutnaBolest().getTerapija().getLekovi().get(0).getNaziv());
    	
	}

	
	@Test
	public void testOdredjivanjeUspesnostiTerapije() {
		Bolest b = new Bolest("Policitemija rubra vera");
    	b.getSimptomi().put("Eritrociti", Status.VERY_HIGH);
    	b.getSimptomi().put("Hematokrit", Status.HIGH);
    	
    	Bolest bo = new Bolest("Tromboza");
    	bo.getSimptomi().put("Eritrociti", Status.VERY_HIGH);
    	bo.getSimptomi().put("Hematokrit", Status.HIGH);
    	
    	
    	
    	Lek l = new Lek("Roferon",12);
    	Lek ll = new Lek("Lek br.2",18);
    	List<Lek> listaZaBazu = new ArrayList<>();
    	listaZaBazu.add(l);
    	listaZaBazu.add(ll);

    	
    	Lek l2 = new Lek("Farin",15);
    	Lek ll2 = new Lek("Lek br.2",18);
    	List<Lek> listaZaBazu2 = new ArrayList<>();
    	listaZaBazu2.add(l2);
    	listaZaBazu2.add(ll2);

    	Terapija te = new Terapija(listaZaBazu);
    	Terapija te2 = new Terapija(listaZaBazu2);
    	
    	Dijagnoza dijZaBazu = new Dijagnoza(b,te);
    	Dijagnoza dijZaBazu2 = new Dijagnoza(bo,te2);
    	
    	Dijagnoza dij = new Dijagnoza(bo, te2);
    	ArrayList<com.sample.bean.Test> testoviTrenutni = napraviTrenutneTestove();
    	dij.setRezultati_nalaza(testoviTrenutni);
    	Pacijent pac = new Pacijent();
    	pac.setBrojGodina(18);
    	pac.setTrenutnaBolest(dij);
    	List<Dijagnoza> istorija = naraviIstorijuBolesti();
    	pac.setIstorijaBolesti(istorija);
    	kSession.insert(dijZaBazu);
		kSession.insert(dijZaBazu2);
		kSession.insert(pac);
		kSession.fireAllRules();
		
		QueryResults results = kSession.getQueryResults("getPacijent");
		
		Pacijent temp = null;
		for(QueryResultsRow row: results) {
			temp = (Pacijent) row.get("$result");
		}
    	
		assertNotNull(temp); 	
		assertTrue(pac.getIstorijaBolesti().get(5).getTerapija().isUspesna());
	}
	

	@Test
	public void testDetekcijaZavisnika() {
		Bolest b = new Bolest("Policitemija rubra vera");
    	b.getSimptomi().put("Eritrociti", Status.VERY_HIGH);
    	b.getSimptomi().put("Hematokrit", Status.HIGH);
    	
    	Bolest bo = new Bolest("Tromboza");
    	bo.getSimptomi().put("Eritrociti", Status.VERY_HIGH);
    	bo.getSimptomi().put("Hematokrit", Status.HIGH);
    	
    	
    	
    	Lek l = new Lek("Roferon",12);
    	Lek ll = new Lek("Lek br.2",18);
    	List<Lek> listaZaBazu = new ArrayList<>();
    	listaZaBazu.add(l);
    	listaZaBazu.add(ll);

    	
    	Lek l2 = new Lek("Farin",15);
    	Lek ll2 = new Lek("Lek br.2",18);
    	List<Lek> listaZaBazu2 = new ArrayList<>();
    	listaZaBazu2.add(l2);
    	listaZaBazu2.add(ll2);

    	Terapija te = new Terapija(listaZaBazu);
    	Terapija te2 = new Terapija(listaZaBazu2);
    	
    	Dijagnoza dijZaBazu = new Dijagnoza(b,te);
    	Dijagnoza dijZaBazu2 = new Dijagnoza(bo,te2);
    	
    	Dijagnoza dij = new Dijagnoza(bo, te2);
    	ArrayList<com.sample.bean.Test> testoviTrenutni = napraviTrenutneTestove();
    	dij.setRezultati_nalaza(testoviTrenutni);
    	Pacijent pac = new Pacijent();
    	pac.setBrojGodina(18);
    	pac.setTrenutnaBolest(dij);
    	List<Dijagnoza> istorija = naraviIstorijuBolesti();
    	pac.setIstorijaBolesti(istorija);
    	kSession.insert(dijZaBazu);
		kSession.insert(dijZaBazu2);
		kSession.insert(pac);
		kSession.fireAllRules();		
    	
		assertTrue(pac.isZavisnik());
	}

	@Test
	public void testOdredjivanjeLoseTerapije() {
		Bolest b = new Bolest("Policitemija rubra vera");
    	b.getSimptomi().put("Eritrociti", Status.VERY_HIGH);
    	b.getSimptomi().put("Hematokrit", Status.HIGH);
    	
    	Bolest bo = new Bolest("Tromboza");
    	bo.getSimptomi().put("Eritrociti", Status.VERY_HIGH);
    	bo.getSimptomi().put("Hematokrit", Status.HIGH);
    	
    	
    	
    	Lek l = new Lek("Roferon",12);
    	Lek ll = new Lek("Lek br.2",18);
    	List<Lek> listaZaBazu = new ArrayList<>();
    	listaZaBazu.add(l);
    	listaZaBazu.add(ll);

    	
    	Lek l2 = new Lek("Farin",15);
    	Lek ll2 = new Lek("Lek br.2",18);
    	List<Lek> listaZaBazu2 = new ArrayList<>();
    	listaZaBazu2.add(l2);
    	listaZaBazu2.add(ll2);

    	Terapija te = new Terapija(listaZaBazu);
    	Terapija te2 = new Terapija(listaZaBazu2);
    	
    	Dijagnoza dijZaBazu = new Dijagnoza(b,te);
    	Dijagnoza dijZaBazu2 = new Dijagnoza(bo,te2);
    	
    	Dijagnoza dij = new Dijagnoza(bo, te2);
    	ArrayList<com.sample.bean.Test> testoviTrenutni = napraviTrenutneLoseTestove();
    	dij.setRezultati_nalaza(testoviTrenutni);
    	Pacijent pac = new Pacijent();
    	pac.setBrojGodina(18);
    	pac.setTrenutnaBolest(dij);
    	List<Dijagnoza> istorija = naraviIstorijuBolesti();
    	pac.setIstorijaBolesti(istorija);
    	kSession.insert(dijZaBazu);
		kSession.insert(dijZaBazu2);
		kSession.insert(pac);
		kSession.fireAllRules();
		
		QueryResults results = kSession.getQueryResults("getPacijent");
		
		Pacijent temp = null;
		for(QueryResultsRow row: results) {
			temp = (Pacijent) row.get("$result");
		}
    	
		assertNotNull(temp); 	
		assertFalse(pac.getIstorijaBolesti().get(5).getTerapija().isUspesna());
		
	}
	
	ArrayList<com.sample.bean.Test> napraviTrenutneLoseTestove(){
		ArrayList<com.sample.bean.Test>testoviTrenutni = new ArrayList<>();

    	testoviTrenutni.add(new com.sample.bean.Test("Eritrociti",2.5f , 5.2f, 0, Status.LOW));
    	testoviTrenutni.add(new com.sample.bean.Test("Leukociti",2.5f , 5.2f, 17, Status.VERY_HIGH));
    	testoviTrenutni.add(new com.sample.bean.Test("Leu",2.5f , 5.2f, 16, Status.VERY_HIGH));
    	return testoviTrenutni;
	}
	

	
	List<Dijagnoza> naraviIstorijuBolesti() {
		Bolest bIstorija = new Bolest("Grip");
    	Lek lIstorija1 = new Lek("Febricet", TipLeka.Analgetik);
    	List<Lek> listaIstorija = new ArrayList<>();
    	listaIstorija.add(lIstorija1);
		Terapija terapijaIstorija = new Terapija(listaIstorija);
    	Dijagnoza dijIstorija1 = new Dijagnoza(LocalDate.of(2021, 03, 01), bIstorija, terapijaIstorija);
    	Dijagnoza dijIstorija2 = new Dijagnoza(LocalDate.of(2021,03,12), bIstorija, terapijaIstorija);
    	Dijagnoza dijIstorija3 = new Dijagnoza(LocalDate.of(2021,04,12), bIstorija, terapijaIstorija);
    	Dijagnoza dijIstorija4 = new Dijagnoza(LocalDate.of(2021,04,28), bIstorija, terapijaIstorija);
    	Dijagnoza dijIstorija5 = new Dijagnoza(LocalDate.of(2021,05,10), bIstorija, terapijaIstorija);
    	Dijagnoza dijIstorija6 = new Dijagnoza(LocalDate.of(2021,05,20), bIstorija, terapijaIstorija);
    	
    	ArrayList<com.sample.bean.Test>testoviZaBazu = new ArrayList<>();
    	
    	testoviZaBazu.add(new com.sample.bean.Test("Eritrociti",2.5f , 5.2f, 1,Status.LOW));
    	testoviZaBazu.add(new com.sample.bean.Test("Leukociti",2.5f , 5.2f, 16,Status.VERY_HIGH ));
    	testoviZaBazu.add(new com.sample.bean.Test("Leu",2.5f , 5.2f, 16,Status.VERY_HIGH ));

    	
    	dijIstorija6.setRezultati_nalaza(testoviZaBazu);
    	List<Dijagnoza> istorijaBolesti = new ArrayList<>();
    	istorijaBolesti.add(dijIstorija1);
    	istorijaBolesti.add(dijIstorija2);
    	istorijaBolesti.add(dijIstorija3);
    	istorijaBolesti.add(dijIstorija4);
    	istorijaBolesti.add(dijIstorija5);
    	istorijaBolesti.add(dijIstorija6);
    	//pac.setIstorijaBolesti(istorijaBolesti);
    	return istorijaBolesti;
	}

	
	ArrayList<com.sample.bean.Test> napraviTrenutneTestove(){
		ArrayList<com.sample.bean.Test>testoviTrenutni = new ArrayList<>();

    	testoviTrenutni.add(new com.sample.bean.Test("Eritrociti",2.5f , 5.2f, 2, Status.LOW));
    	testoviTrenutni.add(new com.sample.bean.Test("Leukociti",2.5f , 5.2f, 15, Status.VERY_HIGH));
    	testoviTrenutni.add(new com.sample.bean.Test("Leu",2.5f , 5.2f, 16, Status.VERY_HIGH));
    	return testoviTrenutni;
	}
}
