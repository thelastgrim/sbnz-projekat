package com.sample.testing;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

import com.sample.DroolsTest;
import com.sample.bean.Dijagnoza;
import com.sample.bean.Pacijent;
import com.sample.bean.SecondaryStatus;
import com.sample.bean.Status;
import com.sample.database.Database;



class TestMain {
	
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
	
	@Test
	void testPrepisivanjeIstihLekovaPrethodnihPacijenata() {
		ArrayList<Pacijent> istorija = Database.readPacijenti();
		ArrayList<com.sample.bean.Test> active_tests = new ArrayList<com.sample.bean.Test>();
		active_tests.add(new com.sample.bean.Test("Eritrociti", 2.5f, 5.2f, 10f));
		active_tests.add(new com.sample.bean.Test("Leukociti", 2.5f, 5.2f, 10f));
		Pacijent trenutni_pacijent = new Pacijent();
    	trenutni_pacijent.setJMBG("123123");
    	trenutni_pacijent.setIstorijaBolesti(DroolsTest.makeIstorijaBolesti());
    	trenutni_pacijent.setTrenutnaBolest(new Dijagnoza(active_tests));
    	kSession.insert(trenutni_pacijent);
    	kSession.insert(istorija);
        kSession.fireAllRules();
        
        QueryResults results = kSession.getQueryResults("getTrenutniPacijent");
        Pacijent p = null;
        
        for ( QueryResultsRow row : results) {
            p = ( Pacijent ) row.get( "$result" );
        }
        
        assertNotNull(p);	
		assertEquals("123123", p.getJMBG());
		assertEquals("Bolest 1", p.getTrenutnaBolest().getBolest().getNaziv());
		assertEquals(3, p.getTrenutnaBolest().getTerapija().getLekovi().size());
		assertEquals("Lek1", p.getTrenutnaBolest().getTerapija().getLekovi().get(0).getNaziv());
		assertEquals("Lek2", p.getTrenutnaBolest().getTerapija().getLekovi().get(1).getNaziv());
		assertEquals("Lek3", p.getTrenutnaBolest().getTerapija().getLekovi().get(2).getNaziv());
		
	}
	
	@Test
	void testHronicnoPovecanIliSmanjenTest() {
		ArrayList<Pacijent> istorija = Database.readPacijenti();
		ArrayList<com.sample.bean.Test> active_tests = new ArrayList<com.sample.bean.Test>();
		active_tests.add(new com.sample.bean.Test("Eritrociti", 2.5f, 5.2f, 10f));
		active_tests.add(new com.sample.bean.Test("Leukociti", 2.5f, 5.2f, 10f));
		Pacijent trenutni_pacijent = new Pacijent();
    	trenutni_pacijent.setJMBG("123123");
    	trenutni_pacijent.setIstorijaBolesti(DroolsTest.makeIstorijaBolesti());
    	trenutni_pacijent.setTrenutnaBolest(new Dijagnoza(active_tests));
    	kSession.insert(trenutni_pacijent);
    	kSession.insert(istorija);
        kSession.fireAllRules();
        
        QueryResults results = kSession.getQueryResults("getTrenutniPacijent");
        Pacijent p = null;
        
        for ( QueryResultsRow row : results) {
            p = ( Pacijent ) row.get( "$result" );
        }
        
        assertNotNull(p);	
		assertEquals("123123", p.getJMBG());
		assertEquals(2, p.getTrenutnaBolest().getRezultati_nalaza().size());
		assertEquals("Eritrociti", p.getTrenutnaBolest().getRezultati_nalaza().get(0).getName());
		assertEquals(SecondaryStatus.CHRONIC, p.getTrenutnaBolest().getRezultati_nalaza().get(0).getSecondaryStatus());
		assertEquals("Leukociti", p.getTrenutnaBolest().getRezultati_nalaza().get(1).getName());
		assertEquals(SecondaryStatus.CHRONIC, p.getTrenutnaBolest().getRezultati_nalaza().get(1).getSecondaryStatus());
		
	}
	
	@Test
	void testOdredjivanjeStatusaTesta() {
		ArrayList<Pacijent> istorija = Database.readPacijenti();
		ArrayList<com.sample.bean.Test> active_tests = new ArrayList<com.sample.bean.Test>();
		active_tests.add(new com.sample.bean.Test("Eritrociti", 2.5f, 5.2f, 0.1f));
		active_tests.add(new com.sample.bean.Test("Leukociti", 2.5f, 5.2f, 2.4f));
		active_tests.add(new com.sample.bean.Test("Hemoglobin", 2.5f, 5.2f, 5.7f));
		active_tests.add(new com.sample.bean.Test("Hematokrit", 2.5f, 5.2f, 123f));
		Pacijent trenutni_pacijent = new Pacijent();
    	trenutni_pacijent.setJMBG("123123");
    	trenutni_pacijent.setIstorijaBolesti(DroolsTest.makeIstorijaBolesti());
    	trenutni_pacijent.setTrenutnaBolest(new Dijagnoza(active_tests));
    	kSession.insert(trenutni_pacijent);
    	kSession.insert(istorija);
        kSession.fireAllRules();
        
        QueryResults results = kSession.getQueryResults("getTrenutniPacijent");
        Pacijent p = null;
        
        for ( QueryResultsRow row : results) {
            p = ( Pacijent ) row.get( "$result" );
        }
        
        assertNotNull(p);	
		assertEquals("123123", p.getJMBG());
		assertEquals(4, p.getTrenutnaBolest().getRezultati_nalaza().size());
		assertEquals("Eritrociti", p.getTrenutnaBolest().getRezultati_nalaza().get(0).getName());
		assertEquals(Status.VERY_LOW, p.getTrenutnaBolest().getRezultati_nalaza().get(0).getStatus());
		assertEquals("Leukociti", p.getTrenutnaBolest().getRezultati_nalaza().get(1).getName());
		assertEquals(Status.LOW, p.getTrenutnaBolest().getRezultati_nalaza().get(1).getStatus());
		assertEquals("Hemoglobin", p.getTrenutnaBolest().getRezultati_nalaza().get(2).getName());
		assertEquals(Status.HIGH, p.getTrenutnaBolest().getRezultati_nalaza().get(2).getStatus());
		assertEquals("Hematokrit", p.getTrenutnaBolest().getRezultati_nalaza().get(3).getName());
		assertEquals(Status.VERY_HIGH, p.getTrenutnaBolest().getRezultati_nalaza().get(3).getStatus());
		
	}

}
