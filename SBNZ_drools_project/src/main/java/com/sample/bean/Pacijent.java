package com.sample.bean;

import java.util.ArrayList;
import java.util.List;


public class Pacijent {
	
	private String JMBG;
	
	private List<Dijagnoza> istorijaBolesti = new ArrayList<Dijagnoza>();
	private Dijagnoza trenutnaBolest;
	private int brojGodina;
	private boolean zavisnik;
	
	public Pacijent() {
		
	}
	
	public Pacijent(List<Dijagnoza> istorijaBolesti, Dijagnoza trenutnaBolest) {
		super();
		this.istorijaBolesti = istorijaBolesti;
		this.trenutnaBolest = trenutnaBolest;
	}
	
	public Pacijent(List<Dijagnoza> istorijaBolesti, Dijagnoza trenutnaBolest, int brojGodina) {
		super();
		this.istorijaBolesti = istorijaBolesti;
		this.trenutnaBolest = trenutnaBolest;
		this.brojGodina = brojGodina;
	}
	public List<Dijagnoza> getIstorijaBolesti() {
		return istorijaBolesti;
	}
	public void setIstorijaBolesti(List<Dijagnoza> istorijaBolesti) {
		this.istorijaBolesti = istorijaBolesti;
	}
	public Dijagnoza getTrenutnaBolest() {
		return trenutnaBolest;
	}
	public void setTrenutnaBolest(Dijagnoza trenutnaBolest) {
		this.trenutnaBolest = trenutnaBolest;
	}

	public String getJMBG() {
		return JMBG;
	}

	public void setJMBG(String jMBG) {
		JMBG = jMBG;
	}

	public int getBrojGodina() {
		return brojGodina;
	}

	public void setBrojGodina(int brojGodina) {
		this.brojGodina = brojGodina;
	}

	public boolean isZavisnik() {
		return zavisnik;
	}

	public void setZavisnik(boolean zavisnik) {
		this.zavisnik = zavisnik;
	}
	
	
	

}
