package com.sample.bean;

import java.util.ArrayList;

public class Lista_dijagnoza {
	
	private ArrayList<Dijagnoza> dijagnoze = new ArrayList<Dijagnoza>();

	public ArrayList<Dijagnoza> getDijagnoze() {
		return dijagnoze;
	}

	public Lista_dijagnoza(ArrayList<Dijagnoza> dijagnoze) {
		super();
		this.dijagnoze = dijagnoze;
	}

	public void setDijagnoze(ArrayList<Dijagnoza> dijagnoze) {
		this.dijagnoze = dijagnoze;
	}
	

}
