package com.sample.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Bolest {
	
	private String naziv;
	
	private HashMap<String, Status> simptomi = new HashMap<String, Status>();

	public Bolest() {
	
	}
	
	public Bolest(String naziv) {
		super();
		this.naziv = naziv;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public HashMap<String, Status> getSimptomi() {
		return simptomi;
	}

	public void setSimptomi(HashMap<String, Status> simptomi) {
		this.simptomi = simptomi;
	}
	
	
	public ArrayList<Test> getSimptomiAsTests(){
		ArrayList<Test> tests = new ArrayList<Test>();
		
		Iterator<Entry<String, Status>> it = simptomi.entrySet().iterator();
    	
        while (it.hasNext()) {
            Map.Entry<String, Status> pair = (Map.Entry<String, Status>)it.next();
            tests.add(new Test(pair.getKey(), pair.getValue()));
            it.remove(); // avoids a ConcurrentModificationException
                
        }
        
        return tests;
		
	}

	@Override
	public String toString() {
		String s =  "Bolest: " + naziv + "\n             simptomi:\n" ;
		for (Map.Entry<String, Status> pair: simptomi.entrySet()) {
			s += "\t"+pair.getKey() + " - " + pair.getValue()+ "\n";
        }

		return s;
	}
	
	
	
	

}
