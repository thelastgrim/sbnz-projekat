package com.sample.bean;

import java.util.ArrayList;

public class Lista_bolesti {
	
	private ArrayList<Bolest> bolesti = new ArrayList<Bolest>();

	public Lista_bolesti() {
		super();
	}

	public Lista_bolesti(ArrayList<Bolest> bolesti) {
		super();
		this.bolesti = bolesti;
	}

	public ArrayList<Bolest> getBolesti() {
		return bolesti;
	}

	public void setBolesti(ArrayList<Bolest> bolesti) {
		this.bolesti = bolesti;
	}
	
	
	
	

}
