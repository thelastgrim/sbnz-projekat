package com.sample.bean;

public class PacijentInfo {
	private String pol;
	private int starost;
	private String JMBG;
	
	public PacijentInfo(String pol, int starost, String jMBG) {
		super();
		this.pol = pol;
		this.starost = starost;
		JMBG = jMBG;
	}
	public PacijentInfo(String jMBG) {
		
		JMBG = jMBG;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public int getStarost() {
		return starost;
	}
	public void setStarost(int starost) {
		this.starost = starost;
	}
	public String getJMBG() {
		return JMBG;
	}
	public void setJMBG(String jMBG) {
		JMBG = jMBG;
	}

}
