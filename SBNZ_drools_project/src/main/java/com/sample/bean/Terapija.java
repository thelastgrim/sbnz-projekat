package com.sample.bean;

import java.util.ArrayList;
import java.util.List;

public class Terapija {
	
	private List<Lek> lekovi = new ArrayList<Lek>();
	private boolean uspesna;
	private Long id;
	
	
	public Terapija() {
		
	}

	public Terapija(List<Lek> lekovi) {
		super();
		this.lekovi = lekovi;
	}

	

	public Terapija(List<Lek> lekovi, boolean uspesna) {
		super();
		this.lekovi = lekovi;
		this.uspesna = uspesna;
	}

	public List<Lek> getLekovi() {
		return lekovi;
	}

	public void setLekovi(List<Lek> lekovi) {
		this.lekovi = lekovi;
	}

	@Override
	public String toString() {
		String s = "Terapija: \n";
		for(Lek l: lekovi) {
			s += "                        "+l.getNaziv() + "\n";
		}
		return s;
	}

	public boolean isUspesna() {
		return uspesna;
	}

	public void setUspesna(boolean uspesna) {
		this.uspesna = uspesna;
	}

	public String getLekoviNames() {
		String lekovi = "";
		for (Lek lek : this.lekovi) {
			lekovi = lekovi + lek.getNaziv()+" ";
		}
		
		return lekovi;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	

}
