package com.sample.bean;

public class Test {
	public String name;
	private float lower;
	private float upper;
	private float value;
	
	public Status status;
	public SecondaryStatus secondaryStatus;
	
	public Test(String name, float lower, float upper, float value) {
		super();
		this.name = name;
		this.lower = lower;
		this.upper = upper;
		this.value = value;
	}
	
	public Test(String name, float lower, float upper, float value, Status status) {
		super();
		this.name = name;
		this.lower = lower;
		this.upper = upper;
		this.value = value;
		this.status = status;
	}
	
	public Test(String name, Status status) {
		super();
		this.name = name;
		this.status = status;
	}

	// 10-20
	/*
	private Status calculateStatus() {
		if(this.value >= this.upper*2) {
			return Status.VERY_HIGH;
		}
		else if (this.value < this.lower/2) {
			return Status.VERY_LOW;
		}
		else if (this.value < this.lower) {
			return Status.LOW;
		}
		else if (this.value > this.upper) {
			return Status.HIGH;
		}else {
			return Status.NORMAL;
		}
	}
*/
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}

	public float getLower() {
		return lower;
	}

	public void setLower(float lower) {
		this.lower = lower;
	}

	public float getUpper() {
		return upper;
	}

	public void setUpper(float upper) {
		this.upper = upper;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Test [name=" + name + ", value=" + value + ", status="
				+ status + "]";
	}

	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}

	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}
	

	

}
