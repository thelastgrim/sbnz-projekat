package com.sample.bean;

public class TerapijaInfo {
	Terapija terapija;
	Long brPojavljivanja;
	
	public TerapijaInfo() {
		
	}
	
	public TerapijaInfo(Terapija terapija, Long brPojavljivanja) {
		super();
		this.terapija = terapija;
		this.brPojavljivanja = brPojavljivanja;
	}
	public Terapija getTerapija() {
		return terapija;
	}
	public void setTerapija(Terapija terapija) {
		this.terapija = terapija;
	}
	public Long getBrPojavljivanja() {
		return brPojavljivanja;
	}
	public void setBrPojavljivanja(Long brPojavljivanja) {
		this.brPojavljivanja = brPojavljivanja;
	}
	
	

}
