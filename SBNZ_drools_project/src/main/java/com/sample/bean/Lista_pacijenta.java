package com.sample.bean;

import java.util.ArrayList;

public class Lista_pacijenta {

	private ArrayList<Pacijent> lista_pacijenata = new ArrayList<Pacijent>();

	public ArrayList<Pacijent> getLista_pacijenata() {
		return lista_pacijenata;
	}

	public void setLista_pacijenata(ArrayList<Pacijent> lista_pacijenata) {
		this.lista_pacijenata = lista_pacijenata;
	}

	public Lista_pacijenta(ArrayList<Pacijent> lista_pacijenata) {
		super();
		this.lista_pacijenata = lista_pacijenata;
	}

	public Lista_pacijenta() {
		super();
	}
	
	
	
}
