package com.sample.bean;

public class Lek {

	private String naziv;
	private TipLeka tipLeka; //zbog detekcije zavisnika da proverimo jel analgetik
	private int uzrastOd; //uzrast kome je lek namenjen(broj godina; npr.za starije od 18 godina)
	public Lek() {
		
	}

	public Lek(String naziv) {
		super();
		this.naziv = naziv;
	}
	
	public Lek(String naziv, TipLeka tipLeka) {
		super();
		this.naziv = naziv;
		this.tipLeka = tipLeka;
	}
	
	public Lek(String naziv, TipLeka tipLeka, int uzrastOd) {
		super();
		this.naziv = naziv;
		this.tipLeka = tipLeka;
		this.uzrastOd = uzrastOd;
	}
	
	public Lek(String naziv,  int uzrastOd) {
		super();
		this.naziv = naziv;
		this.uzrastOd = uzrastOd;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public TipLeka getTipLeka() {
		return tipLeka;
	}

	public void setTipLeka(TipLeka tipLeka) {
		this.tipLeka = tipLeka;
	}

	@Override
	public String toString() {
		return "Lek [naziv=" + naziv + ", tipLeka=" + tipLeka + "]";
	}

	public int getUzrastOd() {
		return uzrastOd;
	}

	public void setUzrastOd(int uzrastOd) {
		this.uzrastOd = uzrastOd;
	}
	
	
	
}
