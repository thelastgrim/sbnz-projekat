package com.sample.bean;

public enum Status {

	VERY_LOW, LOW, NORMAL, HIGH, VERY_HIGH
}
