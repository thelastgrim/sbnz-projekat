package com.sample.bean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class Dijagnoza {
	

	private LocalDate datum; //moramo dodati datum zbog detekcije zavisnika
	private Bolest bolest;
	private ArrayList<Test> rezultati_nalaza =  new ArrayList<Test>();
	private Terapija terapija;
	
	public Dijagnoza(ArrayList<Test> rezultati_nalaza, Bolest bolest, Terapija terapija) {
		super();
		this.bolest = bolest;
		this.terapija = terapija;
		this.rezultati_nalaza = rezultati_nalaza;
		
	}

	public Dijagnoza(ArrayList<Test> rezultati_nalaza, Bolest bolest) {
		super();
		this.bolest = bolest;
		this.rezultati_nalaza = rezultati_nalaza;
	}


	public Dijagnoza(ArrayList<Test> rezultati_nalaza) {
		super();
		this.rezultati_nalaza = rezultati_nalaza;
	}
	
	
	
	public Dijagnoza(LocalDate datum, Bolest bolest, ArrayList<Test> rezultati_nalaza, Terapija terapija) {
		super();
		this.datum = datum;
		this.bolest = bolest;
		this.rezultati_nalaza = rezultati_nalaza;
		this.terapija = terapija;
	}

	public Dijagnoza() {
		
	}
	

	public Dijagnoza(Bolest bolest, Terapija terapija) {
		super();
		this.bolest = bolest;
		this.terapija = terapija;
	}
	
	public Dijagnoza(LocalDate datum, Bolest bolest, Terapija terapija) {
		super();
		this.datum = datum;
		this.bolest = bolest;
		this.terapija = terapija;
	}


	public Dijagnoza(Bolest bolest2) {
		this.bolest = bolest2;
	}

	public Dijagnoza(LocalDate date) {
		this.datum = date;
	}

	public Bolest getBolest() {
		return bolest;
	}


	public void setBolest(Bolest bolest) {
		this.bolest = bolest;
	}


	public Terapija getTerapija() {
		return terapija;
	}


	public void setTerapija(Terapija terapija) {
		this.terapija = terapija;
	}


	public LocalDate getDatum() {
		return datum;
	}


	public void setDatum(LocalDate datum) {
		this.datum = datum;
	}


	@Override
	public String toString() {
		return "Datum: " + datum.getDayOfMonth()+"/"+datum.getMonthValue()+"/"+datum.getYear() + "\n               Dijagnoza: " + bolest.getNaziv() + "\n               " + terapija;
		
		//return "Dijagnoza [datum=" + datum + ", bolest=" + bolest.getNaziv() + ", terapija=" + terapija + "]";
	}


	public ArrayList<Test> getRezultati_nalaza() {
		return rezultati_nalaza;
	}


	public void setRezultati_nalaza(ArrayList<Test> rezultati_nalaza) {
		this.rezultati_nalaza = rezultati_nalaza;
	}
	
	

	

	
	
	

}
