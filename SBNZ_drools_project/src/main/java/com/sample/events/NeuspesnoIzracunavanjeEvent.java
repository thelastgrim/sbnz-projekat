package com.sample.events;


import java.io.Serializable;
import java.util.Date;

import org.kie.api.definition.type.Expires;
import org.kie.api.definition.type.Role;
import org.kie.api.definition.type.Timestamp;



@Role(Role.Type.EVENT)
@Timestamp("executionTime")
@Expires("5m")
public class NeuspesnoIzracunavanjeEvent implements Serializable{

	private static final long serialVersionUID = 1L;
	private Date executionTime;
	
	public NeuspesnoIzracunavanjeEvent() {
		// TODO Auto-generated constructor stub
	}

	public NeuspesnoIzracunavanjeEvent(Date executionTime) {
		super();
		this.executionTime = executionTime;
	}

	public Date getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Date executionTime) {
		this.executionTime = executionTime;
	}


	
	
}
