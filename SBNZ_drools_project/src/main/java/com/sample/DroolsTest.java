package com.sample;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.drools.template.ObjectDataCompiler;
import org.kie.api.KieServices;
import org.kie.api.builder.Results;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.Globals;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;
import org.kie.internal.utils.KieHelper;

import com.google.gson.Gson;
import com.sample.bean.Bolest;
import com.sample.bean.Dijagnoza;
import com.sample.bean.Lek;
import com.sample.bean.Lista_bolesti;
import com.sample.bean.Lista_dijagnoza;
import com.sample.bean.Pacijent;
import com.sample.bean.PacijentInfo;
import com.sample.bean.Status;
import com.sample.bean.Terapija;
import com.sample.bean.TerapijaInfo;
import com.sample.bean.Test;
import com.sample.bean.TipLeka;
import com.sample.database.Database;
import com.sample.events.NeuspesnoIzracunavanjeEvent;

import templateModel.SearchByDateTemplateModel;
import templateModel.SearchByNameTemplateModel;

/**
 * This is a sample class to launch a rule.
 */
public class DroolsTest {
	
    static KieServices ksE = KieServices.Factory.get();
    static KieContainer kContainerE = ksE.getKieClasspathContainer();
	static KieSession kSessionE = kContainerE.newKieSession("ksession-events");
	static KieSession kSession = kContainerE.newKieSession("ksession-rules");
    public static final Pacijent main(HashMap<String, Float> found_tests, PacijentInfo pi) {
    	
    	
    	
    	//String fileName = "C:\\Users\\Aleksandra\\Documents\\GitHub\\sbnz-projekat\\SBNZ_drools_project\\src\\main\\resources\\baza_testova";
    	Gson gson = new Gson();
    	
    	
    	//1. SVI PACIJETI
    	ArrayList<Pacijent> pacijenti = Database.readPacijenti();
    
    	//2. TRENUTNI PACIJENT
    	Pacijent trenutni_pacijent = new Pacijent();
    	trenutni_pacijent.setJMBG(pi.getJMBG());
    	
    	for (Pacijent pacijent : pacijenti) {
			if(pacijent.getJMBG().equals(pi.getJMBG())) {
				trenutni_pacijent.setIstorijaBolesti(pacijent.getIstorijaBolesti());;
			}
		}
    	trenutni_pacijent.setTrenutnaBolest(new Dijagnoza(LocalDate.now()));
    	trenutni_pacijent.getTrenutnaBolest().setRezultati_nalaza(Database.readTests(found_tests));
    	trenutni_pacijent.setBrojGodina(pi.getStarost());
    
    	
    	//trenutni_pacijent.setIstorijaBolesti(makeIstorijaBolesti());
    	
    	//trenutni_pacijent.setTrenutnaBolest(new Dijagnoza(new Bolest("Tromboza"))); umesto setovanja bice preko pravila
    	
    	//3. SVE BOLESTI
    	Lista_bolesti lista_bolest = new Lista_bolesti(Database.readBolesti());
   
    	//4. SVE DIJAGNOZE
    	
    	Lista_dijagnoza lista_dijagnoza = new Lista_dijagnoza(Database.readDijagnoze());
    	
        try {
            // load up the knowledge base
        	NeuspesnoIzracunavanjeEvent event = new NeuspesnoIzracunavanjeEvent(new Date());


         	//kSessionEvents.insert(event);
        	kSessionE.insert(event);
    
        	
        	kSession.insert(pacijenti); // 1. svi pacijenti iz baze
        	kSession.insert(trenutni_pacijent); // 2. trenutni pacijent unesen preko GUI-a
        	kSession.insert(lista_bolest); // 3.sve bolesti iz baze
        	kSession.insert(lista_dijagnoza); // 4. sve dijagnoze iz baze
        	
        		
            kSessionE.fireAllRules();
            kSession.fireAllRules();

            
            
            QueryResults results = kSession.getQueryResults("getTrenutniPacijent");
            Pacijent p = null;
            
            for ( QueryResultsRow row : results) {
                p = ( Pacijent ) row.get( "$result" );
            }
            
            return p;
            
        } catch (Throwable t) {
            t.printStackTrace();
            return null;
        }
    }
    
    /*
    public static ArrayList<Pacijent> makeHistory(){
    	Pacijent p = new Pacijent();
    	p.setJMBG("111111");
    	ArrayList<Test> nalaz = new ArrayList<Test>();
    	nalaz.add(new Test("Eritrociti", 2.5f, 5.2f, 6.6f, Status.HIGH));
    	nalaz.add(new Test("Leukociti", 2.5f, 5.2f, 3.6f, Status.HIGH));
    	
    	List<Lek> lekovi = new ArrayList<Lek>();
    	lekovi.add(new Lek("Lek1"));
    	lekovi.add(new Lek("Lek2"));
    	lekovi.add(new Lek("Lek3"));
    	p.getIstorijaBolesti().add(new Dijagnoza(nalaz, new Bolest("Tromboza"), new Terapija(lekovi, true)));
    	
    	nalaz = new ArrayList<Test>();
    	nalaz.add(new Test("Hemoglobin", 2.5f, 5.2f, 6.7f, Status.HIGH));
    	nalaz.add(new Test("Leukociti", 2.5f, 5.2f, 3.2f, Status.HIGH));
    	
    	p.getIstorijaBolesti().add(new Dijagnoza(nalaz, new Bolest("Bolest 2")));
    	
    	nalaz = new ArrayList<Test>();
    	nalaz.add(new Test("Hematokrit", 2.5f, 5.2f, 6.5f, Status.HIGH));
    	nalaz.add(new Test("Leukociti", 2.5f, 5.2f, 3.0f, Status.HIGH));
    	
    	p.getIstorijaBolesti().add(new Dijagnoza(nalaz, new Bolest("Bolest 3")));
    	
    	ArrayList<Pacijent> prethodni_pacijenti = new ArrayList<>();
    	
    	
    	
    	prethodni_pacijenti.add(p);
    	
    	p = new Pacijent();
    	p.setJMBG("222222");
    	
    	prethodni_pacijenti.add(p);
    	
    	return prethodni_pacijenti;
    	
    	
    }
    */
    
    public static List<Bolest> pretragaBolesti(SearchByNameTemplateModel term){
    	System.out.println("Term u droolsTest: " + term.getNaziv());
    	    	
    	//SVE BOLESTI
    	Lista_bolesti lista_bolest = new Lista_bolesti(Database.readBolesti());
    	if(term.getNaziv().isBlank() || term.getNaziv().isEmpty()) return lista_bolest.getBolesti();  	
   
    	List<Bolest> rezultat = new ArrayList<Bolest>();

	    File f1 = new File("../SBNZ_drools_project/src/main/resources/templates/pretragaBolesti.drt");
		InputStream template = null;
		try {
			template = new FileInputStream(f1);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ObjectDataCompiler converter = new ObjectDataCompiler();
		List<SearchByNameTemplateModel> data = new ArrayList<>();
		data.add(term);
		String drl = converter.compile(data, template);
		/*System.out.println(" = = = = = = = = = = = = = = = =");
        System.out.println(drl);
		System.out.println(" = = = = = = = = = = = = = = = =");*/
		KieSession kieSessionDRT = createKieSessionFromDRL(drl);
		
		for(Bolest bolest: lista_bolest.getBolesti()) { // 3.sve bolesti iz baze
			kieSessionDRT.insert(bolest); 
		}
		
    	
		
		kieSessionDRT.setGlobal("rezultat", rezultat);

		kieSessionDRT.fireAllRules(); 			//OVO OTKOMENTARISI!
	//	System.out.println(" SEARCH \n\n" + drl + "\n\n");

		
		List<String> list= new ArrayList<String>();
	    list.add("a");
	    list.add("b");
	    list.add("c");
		
		Globals globals = kieSessionDRT.getGlobals();
		System.out.println(" ~~~~~~~~~~ " +  globals.get("rezultat") );//
		
		
		return (List<Bolest>) globals.get("rezultat");
    }
    
    
    public static List<Dijagnoza> pretragaIstorijeBolesti(SearchByDateTemplateModel dates, PacijentInfo pi){
    	//SVI PACIJETI
    	ArrayList<Pacijent> pacijenti = Database.readPacijenti();
    
		List<Dijagnoza> istorijaBolesti = new ArrayList<Dijagnoza>();

	    File f2 = new File("../SBNZ_drools_project/src/main/resources/templates/pretragaIstorijeBolesti.drt");
	  		InputStream template2 = null;
		try {
			template2 = new FileInputStream(f2);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ObjectDataCompiler converter2 = new ObjectDataCompiler();
		List<SearchByDateTemplateModel> data2 = new ArrayList<>();
		DateTimeFormatter d; 
		data2.add(new SearchByDateTemplateModel(dates.getPocetniDatum(), dates.getKrajnjiDatum()));

		String drl2 = converter2.compile(data2, template2);
		System.out.println(" = = = = = = = = = = = = = = = =");
        System.out.println(drl2);
		System.out.println(" = = = = = = = = = = = = = = = =");
		KieSession kieSessionByDate = createKieSessionFromDRL(drl2);
		//2. Trazeni pacijent
    	Pacijent trazeniPacijent = new Pacijent();
    	trazeniPacijent.setJMBG(pi.getJMBG());
    	
    	for (Pacijent pacijent : pacijenti) {
			if(pacijent.getJMBG().equals(pi.getJMBG())) {
				trazeniPacijent.setIstorijaBolesti(pacijent.getIstorijaBolesti());;
			}
		}
		if(trazeniPacijent.getIstorijaBolesti().size() == 0) return null;  
    	
		kieSessionByDate.insert(trazeniPacijent);
    	kieSessionByDate.setGlobal("istorijaBolesti", istorijaBolesti);

		kieSessionByDate.fireAllRules(); 			//OVO OTKOMENTARISI!
	//	System.out.println(" SEARCH \n\n" + drl + "\n\n");


		
		Globals globals2 = kieSessionByDate.getGlobals();
		System.out.println(" ~~~~~~~~~~ GLOBALS SERACH BY DATE " +  globals2.get("istorijaBolesti") );
		return (List<Dijagnoza>) globals2.get("istorijaBolesti");
		
		
		
		
		//------------- /SEARCH PO DATUMU ------------- 
    }
    
    
    public static HashMap<String, TerapijaInfo> izvestaj(){
		HashMap<String, TerapijaInfo> izvestaj = new HashMap<>();

    	//1. SVI PACIJETI
    	ArrayList<Pacijent> pacijenti = Database.readPacijenti();
		//4. SVE DIJAGNOZE
    	
    	Lista_dijagnoza lista_dijagnoza = new Lista_dijagnoza(Database.readDijagnoze());
      	kSession.insert(pacijenti); // 1. svi pacijenti iz baze
    	kSession.insert(lista_dijagnoza); // 4. sve dijagnoze iz baze
    	TerapijaInfo najpreporucenija = null;
    	kSession.setGlobal("najpreporucenija",najpreporucenija);
    	
    	TerapijaInfo najmanjePreporucena = null;
    	kSession.setGlobal("najmanjePreporucena",najmanjePreporucena);
    	
    	Long min = 0L;
    	Long max = 0L;
    	
    	kSession.setGlobal("min",min);
    	kSession.setGlobal("max",max);


    	kSession.getAgenda().getAgendaGroup("izvestajOTerapiji").setFocus();
		kSession.fireAllRules();
		izvestaj.put("najpreporucenija",(TerapijaInfo)kSession.getGlobal("najpreporucenija"));
		izvestaj.put("najmanjePreporucena",(TerapijaInfo)kSession.getGlobal("najmanjePreporucena"));
		System.out.println("    drools test min: "+kSession.getGlobal("min") +"    max: "+ kSession.getGlobal("max"));
		
		return izvestaj;
    	
    }
    
    public static List<Dijagnoza> makeIstorijaBolesti(){
    	List<Dijagnoza> istorija_bolest = new ArrayList<Dijagnoza>();
    	List<Lek> listaIstorija = new ArrayList<>();
    	listaIstorija.add(new Lek("Febricet", TipLeka.Analgetik));

    	
    	
    	ArrayList<Test> nalaz = new ArrayList<Test>();
    	nalaz.add(new Test("Eritrociti", 2.5f, 5.2f, 6.6f, Status.HIGH));
    	nalaz.add(new Test("Leukociti", 2.5f, 5.2f, 3.6f, Status.HIGH));
    	
    	istorija_bolest.add(new Dijagnoza(LocalDate.of(2021, 03, 01),  new Bolest("Grip"), nalaz, new Terapija(listaIstorija)));
    	
    	istorija_bolest.add(new Dijagnoza(LocalDate.of(2021,04,12),  new Bolest("Grip"), nalaz, new Terapija(listaIstorija)));
    	
    	istorija_bolest.add(new Dijagnoza(LocalDate.of(2021,05,10),  new Bolest("Grip"), nalaz, new Terapija(listaIstorija)));
    	istorija_bolest.add(new Dijagnoza(LocalDate.of(2021,05,20),  new Bolest("Grip"), nalaz, new Terapija(listaIstorija)));
    	
    	
    	nalaz = new ArrayList<Test>();
    	nalaz.add(new Test("Eritrociti", 2.5f, 5.2f, 6.7f, Status.HIGH));
    	nalaz.add(new Test("Leukociti", 2.5f, 5.2f, 3.2f, Status.HIGH));
    	istorija_bolest.add(new Dijagnoza(LocalDate.of(2021,03,12),  new Bolest("Grip"), nalaz, new Terapija(listaIstorija)));
    	
    	//istorija_bolest.add(new Dijagnoza(nalaz));
    	
    	nalaz = new ArrayList<Test>();
    	nalaz.add(new Test("Eritrociti", 2.5f, 5.2f, 6.5f, Status.HIGH));
    	nalaz.add(new Test("Leukociti", 2.5f, 5.2f, 3.0f, Status.HIGH));
    	//istorija_bolest.add(new Dijagnoza(nalaz));
    	istorija_bolest.add(new Dijagnoza(LocalDate.of(2021,04,28),  new Bolest("Grip"), nalaz, new Terapija(listaIstorija)));
    	
    	return istorija_bolest;
    	
    }
    
    
    private static KieSession createKieSessionFromDRL(String drl){
        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(drl, ResourceType.DRL);
        
        Results results = kieHelper.verify();
        
        if (results.hasMessages(org.kie.api.builder.Message.Level.WARNING, org.kie.api.builder.Message.Level.ERROR)){
            List<org.kie.api.builder.Message> messages = results.getMessages(org.kie.api.builder.Message.Level.WARNING, org.kie.api.builder.Message.Level.ERROR);
            for (org.kie.api.builder.Message message : messages) {
                System.out.println("Error: "+message.getText());
            }
            
            throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }
        
        return kieHelper.build().newKieSession();
    }

    public static class Message {

        public static final int HELLO = 0;
        public static final int GOODBYE = 1;

        private String message;

        private int status;

        public String getMessage() {
            return this.message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getStatus() {
            return this.status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
        
        

    }

}
