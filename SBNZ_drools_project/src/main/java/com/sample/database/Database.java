package com.sample.database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.sample.bean.Bolest;
import com.sample.bean.Dijagnoza;
import com.sample.bean.Pacijent;
import com.sample.bean.Test;

public class Database {
		private static String fileName = "../SBNZ_drools_project/src/main/resources/baza_testova";
		//private static String fileName = "C:\\Users\\Aleksandra\\Documents\\GitHub\\sbnz-projekat\\SBNZ_drools_project\\src\\main\\resources\\baza_testova";
		
		private static String pacijenti_file = "../SBNZ_drools_project/src/main/resources/pacijenti.json";
		//private static String pacijenti_file = "C:\\Users\\Aleksandra\\Documents\\GitHub\\sbnz-projekat\\SBNZ_drools_project\\src\\main\\resources\\pacijenti.json";
		
		private static String bolesti_file = "../SBNZ_drools_project/src/main/resources/bolesti.json";
		//private static String bolesti_file = "C:\\Users\\Aleksandra\\Documents\\GitHub\\sbnz-projekat\\SBNZ_drools_project\\src\\main\\resources\\bolesti.json";
		
		private static String dijagnoze_file = "../SBNZ_drools_project/src/main/resources/dijagnoze.json";
		//private static String dijagnoze_file = "C:\\Users\\Aleksandra\\Documents\\GitHub\\sbnz-projekat\\SBNZ_drools_project\\src\\main\\resources\\dijagnoze.json";
		
	
	public static ArrayList<Pacijent> readPacijenti(){
		
		ArrayList<Pacijent> pacijenti = new ArrayList<Pacijent>();
		
		Pacijent[] pacijentiArray = new Gson().fromJson(readFileToString(pacijenti_file), Pacijent[].class);
		for (Pacijent pacijent : pacijentiArray) {
			pacijenti.add(pacijent);
		}
		
		return pacijenti;
	}
	
	public static ArrayList<Bolest> readBolesti(){
		
		ArrayList<Bolest> bolesti = new ArrayList<Bolest>();
		
		Bolest[] bolestiArray = new Gson().fromJson(readFileToString(bolesti_file), Bolest[].class);
		for (Bolest bolest: bolestiArray) {
			bolesti.add(bolest);
		}
		
		return bolesti;
		
	}
	
	public static ArrayList<Dijagnoza> readDijagnoze(){
		
		ArrayList<Dijagnoza> dijagnoze = new ArrayList<Dijagnoza>();
		
		Dijagnoza[] dijagnozeArray = new Gson().fromJson(readFileToString(dijagnoze_file), Dijagnoza[].class);
		for (Dijagnoza dijagnoza: dijagnozeArray) {
			dijagnoze.add(dijagnoza);
		}
		
		return dijagnoze;
		
	}
	
	public static ArrayList<Test> readTests(HashMap<String, Float> found_tests){
		ArrayList<Test> active_tests = new ArrayList<>();
		File file= new File(fileName);    //creates a new file instance  
    	FileReader fr;
		try {
			fr = new FileReader(file);
			BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream  
	    	String line;  
	    	while((line=br.readLine())!=null)  {  
	        	String[] values = line.split(",");
	        	Iterator<Entry<String, Float>> it = found_tests.entrySet().iterator();
	        	
	            while (it.hasNext()) {
	                Map.Entry<String, Float> pair = (Map.Entry<String, Float>)it.next();
	               
	                if(values[0].equals(pair.getKey())) {
	                	active_tests.add(new Test(values[0], Float.valueOf(values[1]), Float.valueOf(values[2]), pair.getValue()));
	                	it.remove(); // avoids a ConcurrentModificationException
	                }
	                
	            }
	    	}  
	    	fr.close();    //closes the stream and release the resources
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   //reads the file  
    	
    	return active_tests;
	}
	
	
	private static String readFileToString(String filename) {
		
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(filename));
			StringBuilder stringBuilder = new StringBuilder();
			String line = null;
			String ls = System.getProperty("line.separator");
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}
			// delete the last new line separator
			stringBuilder.deleteCharAt(stringBuilder.length() - 1);
			reader.close();

			String content = stringBuilder.toString();
			
			return content;
				
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}

}
