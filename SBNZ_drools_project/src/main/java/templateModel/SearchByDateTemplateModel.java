package templateModel;

import java.time.LocalDate;

public class SearchByDateTemplateModel {
	private String pocetniDatum;
	private String krajnjiDatum;
	

	public SearchByDateTemplateModel() {
		
	}
	
	public SearchByDateTemplateModel(String pocetniDatum, String krajnjiDatum) {
		this.pocetniDatum = pocetniDatum;
		this.krajnjiDatum = krajnjiDatum;
	}
	public String getPocetniDatum() {
		return pocetniDatum;
	}
	public void setPocetniDatum(String pocetniDatum) {
		this.pocetniDatum = pocetniDatum;
	}
	public String getKrajnjiDatum() {
		return krajnjiDatum;
	}
	public void setKrajnjiDatum(String krajnjiDatum) {
		this.krajnjiDatum = krajnjiDatum;
	}
	
	

}
